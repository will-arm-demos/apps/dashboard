# Kubernetes files

Made specifically for deployments such as the BuzzCrate project. 

## Use 

After the build process, the deployment should be simply: 
```bash
kubectl apply -f k3s-deployment.yaml
```

Everything should be working as intended afterwards. 

### Changing Websocket URL and Port

To change the URL port and Websocket, the environment variables must be set for:
* `SERVER_URL`
* `SERVER_PORT`

The connection string must be for the host [BuzzChat](https://gitlab.com/buzzcrate/apps/buzzchat) instance that the app section wants to connect too.

These enviornment variables are configured in Kubernetes via a ConfigMap.


## Kustomize

This is considered to be the base folder for Kustomize and is set in anticipation for you.

