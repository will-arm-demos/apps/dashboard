[[_TOC_]]

# Original Overlay

When doing some testing, I wanted to play with Kustomize to make my life easier and to have as a simple piece for giving a future talk to non-k8s people. So if this is too basic then good! Share it with others. 

## To use

Only 1 file is added now and the paths are set in the repo to apply to the K8s cluster. Simply modify what you want in the `configmap.yaml`. Feel free to add files as needed but make sure they're set inside of the `kustomization.yaml` file listed in this folder.

## Validate

To validate, Kustmomize does need to be installed. Local to this folder, run `kustomize build .`. Replace `.` with the path if in a different folder (relative or absolute).

## Applying

There are 2 simple ways to apply Kustomize anymore. They are as follows.

**With Kustomize installed**:
```bash
kustomize apply .
```

**With Kubectl only**:
```bash
kubectl apply -k .
```

Replace `.` if in a differnt folder to the path of this folder.
