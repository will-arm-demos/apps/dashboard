package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

var (
	SERVER_URL    = os.Getenv("SERVER_URL")  // Server URL dns or IP address
	SERVER_PORT   = os.Getenv("SERVER_PORT") // Server port if needed
	SERVER_STRING string
)

func init() {
	// Check the server URL and set the DNS name if not explicit
	if SERVER_URL == "" {
		SERVER_URL = "staticphantom.com"
	}

	if SERVER_PORT == "" {
		SERVER_PORT = "80"
	}

	SERVER_STRING = fmt.Sprintf("%s:%s", SERVER_URL, SERVER_PORT)
}

type HeaderTemplate struct {
	HomeActive    bool
	DashActive    bool
	AppActive     bool
	LoginActive   bool
	BodyParagraph string
	ServerUrl     string
}

func htHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/ht" && r.URL.Path != "/" {
		fmt.Printf("Path not found: %s\n", r.URL.Path)
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		fmt.Printf("Method in ht: %s\n", r.Method)
		http.Error(w, "method is not supported", http.StatusNotFound)
		return
	}
	tmpl, tmplErr := template.ParseFiles("templates/ht.html")
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}

	data := HeaderTemplate{
		HomeActive:    true,
		DashActive:    false,
		AppActive:     false,
		LoginActive:   false,
		BodyParagraph: "Home string.",
		ServerUrl:     SERVER_STRING,
	}

	errEx := tmpl.Execute(w, data)
	if errEx != nil {
		log.Println(errEx)
	}
}

func main() {
	PORT := ":8001"
	arguments := os.Args
	if len(arguments) == 2 {
		PORT = ":" + arguments[1]
	}
	fmt.Printf("Using default port number: %s\n", PORT)
	fmt.Printf("App will connection to %s:%s\n", SERVER_URL, SERVER_PORT)

	fileServer := http.FileServer(http.Dir("./static"))
	http.Handle("/", fileServer)
	http.HandleFunc("/ht", htHandler)
	http.HandleFunc("/dashboard", dashboardHandler)
	http.HandleFunc("/d3arc.js", libD3arcHandler)
	http.HandleFunc("/app", appHandler)

	if err := http.ListenAndServe(PORT, nil); err != nil {
		log.Fatal(err)
	}
}
